﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlayer : MonoBehaviour

{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.GetComponent<Player>();
        if (player == null)
        {
            return;
        }

        if (player.state == PlayerState.Dead)
        {
            return;
        }

        
        player.Kill();

        var ui = FindObjectOfType<UIGameOver>();
        ui.Show();

        
    }
}

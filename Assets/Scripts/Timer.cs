﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    [SerializeField] private Text uiText;
    [SerializeField] private float mainTimer;

    private float timer;
    private bool canCount = true;
    private bool doOnce = false;
    public bool timerPaused = false;

    public void Start()
    {
        //timerPaused = false;
        timer = mainTimer;
    }
    

void Update()
    {
        
        if(timer >= 0.0f && canCount)
        {
            timer -= Time.deltaTime;
            uiText.text = timer.ToString("F");
        }

        else if(timer <= 0.0f && !doOnce)
        {
            
            canCount = false;
            doOnce = true;
            uiText.text = "0.00";
            timer = 0.0f;
            //var player = gameObject.GetComponent<Player>();
            //player.Kill();
            var player = FindObjectOfType<Player>();
            player.Kill();

        }

        if (timerPaused)
        {
            timer += Time.deltaTime;
        }
    }

    public void Stop()
    {
        timerPaused = true;
        timer += Time.deltaTime;
    }

    public void ResetTimer()
    {
        timerPaused = false;
        timer = mainTimer;
        canCount = true;
        doOnce = false;
    }
    
}

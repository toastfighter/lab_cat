﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR  //nur, wenn unity editor läuft
using UnityEditor;
#endif

public class UIStartScene : UIScene
{

	// Use this for initialization
	void Start ()
    {
		
	}

    public void StartGame()
    {

        Hide();

        var player = FindObjectOfType<Player>();
        player.StartGame();
    }

    public void ExitGame()
    {
        Hide();

#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif

    }
	
}

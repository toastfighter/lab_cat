﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Finish : MonoBehaviour

{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.GetComponent<Player>();
        if (player == null)
        {
            return;
        }

        player.enabled = false;

        var ui = FindObjectOfType<LevelCompleted>();
        ui.Show();

        var timer = FindObjectOfType<Timer>();
        timer.Stop();

        var playlist = FindObjectOfType<Playlist>();

        playlist.Win();

    }
}


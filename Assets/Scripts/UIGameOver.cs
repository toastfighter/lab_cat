﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR  //nur, wenn unity editor läuft
using UnityEditor;
#endif

[RequireComponent(typeof(CanvasGroup))]
public class UIGameOver : UIScene
{
    private void Start()
    {
        Hide();
    }

    public void ExitGame()
    {
        Hide();
        FindObjectOfType<UIStartScene>().Show();
    }

    public void RestartGame()
    {
        var player = FindObjectOfType<Player>();
        player.StartGame();
        Hide();
    }
}

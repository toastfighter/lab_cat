﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : MonoBehaviour {

    public PlayerState state { get; private set; }

    private bool isGrounded = false; //benennung so, dass häufigste antowrt ja/true ist. daher isGrounded und nicht isnotGrounded

    private Rigidbody2D rigid;

    [SerializeField]
    private float speed = 7f;

    [SerializeField]
    private float runMoveCoefficient = 1f;

    [SerializeField]
    private Vector2 jumpVector = Vector2.up;

    [SerializeField]
    private float jumpForce = 500f;

    [SerializeField]
    private Animator animator;
    
    private AudioSource audioPlayer;

    private bool isDying = false;

    private bool isJumping;

    private Collider2D collider;

    private Vector3 startPosition;

    public GameObject TimerController;

    public GameObject EnemyBlob;

    public GameObject GameTheme;

    [SerializeField]
    private ParticleSystem particle;


    public void Awake()
    {
        startPosition = transform.position;
        rigid = GetComponent<Rigidbody2D>();
        audioPlayer = GetComponent<AudioSource>();
        collider = GetComponent<Collider2D>();
    }

    public void StartGame()
    {

        particle.Stop();
        enabled = true;

        GameTheme.SetActive(true);
        EnemyBlob.SetActive(true);
        TimerController.SetActive(true);

        transform.position = startPosition;
        state = PlayerState.Alive;
        isDying = false;

        rigid.isKinematic = false;
        rigid.velocity = Vector2.zero;

        animator.SetTrigger("Reset");
    }

    public void Die()
    {
        var timer = FindObjectOfType<Timer>();
        timer.Stop();

        var playlist = FindObjectOfType<Playlist>();
        playlist.Die();

        var ui = FindObjectOfType<UIGameOver>();
        ui.Show();

        enabled = false;
        isDying = true;
        GameTheme.SetActive(false);
        
        animator.SetTrigger("Die!");

        particle.Play();
    }

    public void Kill()
    {
        state = PlayerState.Dead;

        //rigid.isKinematic = true;
        rigid.velocity = Vector2.zero;

        Die();
    }

    void Update () 
    {

        if (isDying == true)
        {
            if (audioPlayer.isPlaying)
            {
                return;
            }

            //gameObject.SetActive(false);
            return;
        }

        var position = transform.position;
        var deltaTime = Time.deltaTime;

        Debug.DrawRay(transform.position, Vector2.down, Color.red);


        var hit = Physics2D.Raycast(transform.position, Vector2.down, 100f, LayerMask.GetMask("Ground")); //ray, der nach unten zeigt. theoretisch unendlich lang.
        
        //if (state == PlayerState.Dead)
        //{
        //    return;
        //}

        var wasGrounded = isGrounded;
        isGrounded = hit.distance < 1.09f; //1.09 = hälfte unserer größe, weil von der mitte aus gemessen wird
        

        var movementx = Input.GetAxis("Horizontal"); //Bewegung nach links/rechts: -1 oder +1
        rigid.velocity = new Vector2(movementx * speed, rigid.velocity.y); //Beschleunigung
        animator.SetBool("IsWalking", movementx != 0);

        var newRotation = gameObject.transform.localRotation.eulerAngles;

        if(movementx > 0)
        {
            newRotation.y = 0f;
        }

        else if(movementx < 0)
        {
            newRotation.y = 180f;
        }
       
        gameObject.transform.rotation = Quaternion.Euler(newRotation);
        
        var movementy = rigid.velocity.y;
        animator.SetBool("IsJumping", movementy != 0f);


        if (isGrounded) //jump if grounded
        {
            if (Input.GetAxis("Vertical") > 0f && !isJumping)
            {

                isJumping = true;

                rigid.velocity = new Vector2(movementx * speed, jumpForce);

                var playlist = FindObjectOfType<Playlist>();

                playlist.Jump();
            }
        }
        if (!wasGrounded && isGrounded && isJumping)
        {
            isJumping = false;
        }
    }
}
public enum PlayerState
{
    Alive,

    Dead
}
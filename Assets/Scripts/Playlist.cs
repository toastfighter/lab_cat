﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playlist : MonoBehaviour
{
    [SerializeField]
    private AudioClip die;

    [SerializeField]
    private AudioClip jump;

    [SerializeField]
    private AudioClip walk;

    [SerializeField]
    private AudioClip win;

    //[SerializeField]
    //private AudioClip theme;

    //public void Theme()
    //{
    //    var audioSource = GetComponent<AudioSource>();
    //    audioSource.clip = theme;
    //    audioSource.Play();
    //}

    public void Die()
    {
        var audioSource = GetComponent<AudioSource>();
        audioSource.clip = die;
        audioSource.Play();
    }
	
    public void Jump()
    {
        var audioSource = GetComponent<AudioSource>();
        audioSource.clip = jump;
        audioSource.Play();
    }

    public void Walk()
    {
        var audioSource = GetComponent<AudioSource>();
        audioSource.clip = walk;
        audioSource.Play();
    }

    public void Win()
    {
        var audioSource = GetComponent<AudioSource>();
        audioSource.clip = win;
        audioSource.Play();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Vector2 direction = Vector2.left;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.GetComponent<Player>();

        if ( player == null)
        {
            return;
        }

        this.enabled = false;
        player.Die();
    }

    private void Update()
    {
        var ray = Physics2D.Raycast(transform.position, Vector2.down + direction, 1f, LayerMask.GetMask("Ground"));

        Debug.DrawRay(transform.position, Vector2.down + direction, Color.red);

        if (ray.transform == null)
        {
            direction = direction * -1;
        }
        else
        {
            var position = transform.position;
            position.x += direction.x * 2f * Time.fixedDeltaTime;
            transform.position = position;
        }
    }
}

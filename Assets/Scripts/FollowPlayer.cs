﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    
    private Player target;

    [SerializeField]
    private Vector3 offset = Vector3.back * 10;

    void Awake ()
    {
        target = FindObjectOfType<Player>();
    }

	void Update ()
    {
        transform.position = target.transform.position + offset;
    }
}
